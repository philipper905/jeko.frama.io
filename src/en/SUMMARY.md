[Home](./home.md)

- [Install](./install.md)
- [Edit](./edit.md)
    - [Emacs](./emacs.md)
- [Hello World](./hello.md)
- [Data types](./data-types.md)
    - [Booleans](./booleans.md)
    - [Numbers](./numbers.md)
	- [Characters](./char.md)
