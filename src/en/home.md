![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Background

Here, you will find resources to introduce you to Guile (and indirectly to the functional paradigm) **without stress and with tests !**

Learning a new programming language is a tedious undertaking. In the case of Guile, we find ourselves immersed in an ancestral literature: that of the Lisp family.

As far as I'm concerned, I had a hard time finding the resources to learn how to program with the Guile language specifically. I had no experience with Lisp and its dialects (Scheme). I found myself navigating through a very diffuse flow of information, which did not make my learning easier.

With the **Guile hacker's handbook**, I want to concentrate everything that I think a hacker apprentice would need to go from beginner (discovering syntax) to intermediate (creating simple programs).

## A complement to the Scheme language literature

Scheme is a dialect of the Lisp programming language. Its specification evolves at the rate of revisions.

Guile is an implementation of the Scheme language.

Thus, the resources for learning Guile are mixed with those for learning Scheme or Lisp. But they diverge in terms of features/api/modules.

The **Guile hacker's handbook** is a resource dedicated specifically to Guile. All the features/modules used in this book are part of the Guile distribution.

## A complement to the Guile language reference

As its name suggests, a reference is a work that is regularly returned to with a very specific need. It provides an exhaustive list of the functionalities of the language without explaining how to implement them.

The **Guile hacker's handbook** illustrates these concepts through structured and concrete situations.

## What do you need

- A computer and the GNU/Linux system of your choice. 
- Some programming experience.
- The desire to learn.

## Feedbacks

Please, help me improve this book! Give me all your comments:
- did I skip a step?
- any inconsistencies?
- need more information?
- something doesn't work?
- spelling mistakes?
- not enough love?
- You dig it?

Send it all to me:
- [email](mailto:jeremy@korwin-zmijowski.fr)
- [twitter](https://twitter.com/JeremyKorwin)
- [linkedin](www.linkedin.com/in/jeko)
- [mastodon](@jeko@framapiaf.org)
- [instagram](https://www.instagram.com/jeremy_kz/)
- by opening ticket on the [repository](https://framagit.org/Jeko/jeko.frama.io)

*Inspired by : quii.gitbook.io*
