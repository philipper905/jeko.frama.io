# Emacs

[Emacs](https://www.gnu.org/software/emacs/) est l’éditeur qui fourni le meilleur support pour Guile.

## Installation

Tu peux l’installer de la même manière que tu as installé Guile (ci-dessous, l'exemple avec Guix) :

```bash
$ guix install emacs
```

## Configuration

Cet ouvrage n'a pas pour vocation à apprendre à utiliser Emacs. Je te propose donc ci-suivant une configuration prêt à l'emploi pour rendre l'écriture de programmes en Guile plus agréable avec Emacs. Libre à toi de ne prendre que ce qui t'intéresse !

### Aide visuelle

#### Surligner la ligne du curseur

```lisp
;; paste this lines into your emacs config file
(global-hl-line-mode +1)
```

#### Surligner la paire de délimiteurs sous le curseur

```lisp
;; paste this lines into your emacs config file
(show-paren-mode 1)
(setq show-paren-delay 0)
```

#### Ajouter de la couleur aux paires de délimiteurs successifs

*Installation*

```bash
$ guix install emacs-rainbow-delimiters
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
```

### Aide opérationnelle

#### « make Scheme hacking inside Emacs (even more) fun »

Si tu ne devais installer qu'une extension, c'est bien celle-ci.

*Installation*

```bash
$ guix install emacs-geiser
```

*Activation*

`M-x run-guile`

*Utilisation*

Tu peux te référer à [https://www.nongnu.org/geiser/geiser_5.html#Cheat-sheet](https://www.nongnu.org/geiser/geiser_5.html#Cheat-sheet)

#### Ajouter de l'auto-completion à Geiser :

*Installation*

```bash
$ guix install emacs-ac-geiser
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(ac-config-default)
(require 'ac-geiser)
(add-hook 'geiser-mode-hook 'ac-geiser-setup)
(add-hook 'geiser-repl-mode-hook 'ac-geiser-setup)
(eval-after-load "auto-complete"
  '(add-to-list 'ac-modes 'geiser-repl-mode))
```

#### Éditer le code en s'appuyant sur la structure des S-expressions

*Installation*

```bash
$ guix install emacs-paredit
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'paredit)
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'scheme-mode-hook #'enable-paredit-mode)
```

*Utilisation*

Je ne saurais faire mieux que [ce guide de Dan Midwood](http://danmidwood.com/content/2014/11/21/animated-paredit.html).

### Aide au réusinage

#### Éditer plusieurs chaine de charactères de la même manière simultanément

*Installation*

```bash
$ guix install emacs-iedit
```
*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'iedit)
```

*Utilisation*

`C-;` sur un mot pour éditer toutes ses occurences.  
`C-0 C-;` sur un mot pour éditer toutes ses occurences dans la région active.

#### Éditer plusieurs endroits du fichier de la même manière simultanément

 *Installation*

```bash
$ guix install emacs-multiple-cursors
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'multiple-cursors)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
```

*Utilisation*

`C->` Ajouter un curseur sur la ligne suivante  
`C-<` Ajouter un curseur sur la ligne précédente

Lorsque l'édition est finie, `C-g`

#### Extraire des variables et des fonctions


*Installation*

```lisp
;; paste this lines into your emacs config file
(if (not (package-installed-p 'emr))
    (progn
      (package-refresh-contents)
      (package-install 'emr)))
```

*Activation*

```lisp
;; paste this lines into your emacs config file
(require 'emr)
(autoload 'emr-show-refactor-menu "emr")
(define-key prog-mode-map (kbd "M-RET") 'emr-show-refactor-menu)
(eval-after-load "emr" '(emr-initialize))
(global-set-key (kbd "M-v") 'emr-scm-extract-variable)
(global-set-key (kbd "M-f") 'emr-scm-extract-function)
```

*Utilisation*

`M-v` Extraire une variable  
`M-f` Extraire une fonction
