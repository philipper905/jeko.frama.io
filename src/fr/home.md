![](https://www.gnu.org/software/guile/static/base/img/kid-programming-a-robot.png)

# Contexte

Ici, tu trouveras des ressources pour t'initier à Guile (et indirectement au paradigme fonctionnel) **sans stress et avec des tests !**

Apprendre un nouveau langage de programmation est une entreprise fastidieuse. Dans le cas de Guile, on se retrouve plongé dans une littérature ancestrale : celle de la famille des Lisp.

En ce qui me concerne, j'ai eu beaucoup de mal à trouver les ressources me permettant d'apprendre à programmer avec le langage Guile spécifiquement. Je n'avais aucune expérience avec Lisp et ses dialectes (Scheme). Je me suis retrouvé à naviguer dans un flot d'informations très diffus, ce qui n'a pas facilité mon apprentissage.

Avec le **Carnet du hacker Guile**, je veux concentrer tout ce dont un apprenti hacker aurait, selon moi, besoin pour passer du stade de débutant (qui découvre la syntaxe) à celui d'intermédiaire (qui créé des programmes simples).

## Un complément à la littérature du langage Scheme

Scheme est un dialecte du langage de programmation Lisp. Sa spécification évolue au rythme de révisions.

Guile est une implémentation du langage Scheme.

Ainsi, les ressources pour apprendre Guile se mêlent avec celles pour apprendre Scheme ou Lisp. Mais elles divergent en en termes de fonctionnalités/api/modules.

Le **Carnet du hacker Guile** est une ressource dédiée spécifiquement à Guile. Tou(te)s les fonctions/modules utilisés dans cet ouvrage font parti de la distribution de Guile.

## Un complément à la référence du langage Guile

Comme son nom l'indique, une référence est un ouvrage sur lequel on revient régulièrement avec un besoin bien précis. Elle fourni la liste exhaustive des fonctionnalités du langage sans pour autant expliquer comment les mettre en oeuvre.

Le **Carnet du hacker Guile** illustre ces concepts par des mises en situations structurées et concrètes.

## De quoi tu auras besoin

- Un ordinateur et le système GNU/Linux de ton choix. 
- Un peu d'expérience en programmation.
- L'envie d'apprendre.

## Retours

S'il te plait, aide-moi à améliorer cet ouvrage ! Fais moi part de toutes tes remarques :
- est-ce que j'ai sauté une étape ?
- des incohérences ?
- besoin d'information supplémentaires ?
- quelque chose ne fonctionne pas ?
- des fautes d'orthographe ?
- pas assez d'amour ?
- tu kiff à donf ?

Envoies-moi tout ça :
- [email](mailto:jeremy@korwin-zmijowski.fr)
- [twitter](https://twitter.com/JeremyKorwin)
- [linkedin](www.linkedin.com/in/jeko)
- [mastodon](@jeko@framapiaf.org)
- [instagram](https://www.instagram.com/jeremy_kz/)
- en ouvrant un ticket sur le [dépôt](https://framagit.org/Jeko/jeko.frama.io)

*Inspiré par : quii.gitbook.io*
